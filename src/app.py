from datetime import date

import tornado
from tornado.options import define, options
from tornado.web import MissingArgumentError

from loader.image import Image

from loader.rabbit import Messaging

define("port", default=8888, help="run on the given port", type=int)

messenger = Messaging()


class Application(tornado.web.Application):

    def __init__(self):
        handlers = [
            (r'/upload', UploadHandler)  # Point d'accès des uploads
        ]
        tornado.web.Application.__init__(self, handlers)


class UploadHandler(tornado.web.RequestHandler):

    def data_received(self, chunk):
        pass

    def post(self):  # POST /upload

        try:
            nom = self.get_argument('nom')
        except MissingArgumentError:
            nom = None

        try:
            tags = self.get_argument('tags')
            if tags:
                tags = tags.split(',')
            else:
                tags = []
        except MissingArgumentError:
            tags = []

        try:
            operations = self.get_argument('operations')
            if operations:
                operations = operations.split(',')
            else:
                operations = []
        except MissingArgumentError:
            operations = []

        description = self.get_argument('description')
        date_create = date.today()
        categorie = self.get_argument('categorie')

        for _, files in self.request.files.items():
            for info in files:
                filename = info["filename"]
                body = info["body"]

                output_file = open("upload/" + filename, "wb")
                output_file.write(body)

                input_file = open("upload/" + filename, "rb")

                image = Image(
                    description=description,
                    categorie=categorie,
                    tags=tags,
                    date=date_create,
                    nom=nom,
                    file=input_file,
                    operations=operations)

                messenger.send(image)

        self.write("OK, DONE")


def main():
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
