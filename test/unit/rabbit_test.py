from unittest import TestCase

from src.loader.image import Image
from src.loader.rabbit import Messaging


class ImageTest(TestCase):

    def test_send_to_save(self):
        file = open("resources/image.jpeg", "rb")
        image = Image(nom="image",
                      description="une description",
                      file=file,
                      categorie="test"
                      )

        messenger = Messaging()
        queue_name = messenger.send(image)
        self.assertEqual("to_save", queue_name, "L'image n'ayant pas d'opérations elle doit être sauvée")