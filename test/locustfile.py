import json

from locust import TaskSet, task, HttpLocust, between


class PostTaskSet(TaskSet):

    @task
    def post_image(self):
        url = 'http://127.0.0.1:8080/upload'
        image = open("resources/image.jpeg", "rb")
        files = {"media": image}

        tags = ["test"]
        properties = {}
        operations = ["tagger"]
        self.client.post(url, files=files, data={"tags": ','.join(tags), "categorie": "categorie", "nom": "test.jpeg", "description": "une description"})


class MyLocust(HttpLocust):
    task_set = PostTaskSet
    wait_time = between(0.0, 0.0)