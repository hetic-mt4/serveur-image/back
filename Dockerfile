FROM python:3.7

ADD src/ /
ENV RABBIT_HOST="localhost"

VOLUME upload

EXPOSE 8888

RUN pip install tornado pika

ENTRYPOINT ["python", "app.py"]